from flask import Flask
from flask import Response
from flask import request
import requests

app = Flask(__name__)
app.debug = True
@app.route('/<path:path>', methods=['POST'])
def _proxy(*args, **kwargs):
  method=request.method
  url=request.url.replace(
      '127.0.0.1:5000', 'api.playtogga.com'
      ).replace('http','https')
  data = {}
  for k, v in dict(request.form).iteritems():
    data[k] = v[0]

  cookies=request.cookies

  headers = {'Accept': 'application/json', 'User-Agent': 'Mozilla/5.0'}

  app.logger.info([method, url, headers, data, cookies])

  resp = requests.post(url, data=data, headers=headers, cookies=cookies)
  app.logger.info(resp.content)
  response_cookies = requests.utils.dict_from_cookiejar(resp.cookies)

  response = Response(resp.content, resp.status_code, headers)
  for k, v in response_cookies.iteritems():
    response.set_cookie(k, value=v)
  return response

